let token = localStorage.getItem('token');
let adminUser = localStorage.getItem('isAdmin');
let courseContainer = document.querySelector('#coursesContainer');
let buttonControl = document.querySelector('#buttonControl');
let courseData;
let cardFooter;

// If not logged in

if (!token || token === null){
    
buttonControl.innerHTML = `<a href="../index.html" class="btn btn-primary mt-3 buttonEffect">
      Home Page </a>`;

fetch('https://rocky-harbor-60941.herokuapp.com/api/courses', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        }

    })

    .then(res => res.json())
    .then(data => {

        if (data.length<1){
            courseData = "No Courses Available."
        };

        courseData = data.reverse().map(courseData => {
            
            cardFooter = `<a href="./register.html" class="btn btn-primary text-white btn-block">Sign Up</a>`;

            return (
                `
                <div class="col-md-6 my-3">
                        <div class="card shadow border border-secondary">
                            <div class="card-body">
                                <h5 class="card-title">${courseData.name}</h5>
                                <p class="card-text text-left">${courseData.description}</p>
                                <p class="card-text text-right">&#8369;${courseData.price}</p>
                            </div>
                            <div class="card-footer">${cardFooter}</div>
                        </div>
                    </div> 
                `)
        }).join("");


        courseContainer.innerHTML = courseData;
    })

// if logged in check if admin 

} else {

// If logged in as Admin

if (adminUser === "true") {



	buttonControl.innerHTML = `<a href="./addCourse.html" class="btn btn-primary mt-3 buttonEffect">
      Add Course </a>`;

    fetch('https://rocky-harbor-60941.herokuapp.com/api/courses', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        }

    })


    .then(res =>res.json())
        .then(data =>{
            if(data.length<1){
                courseData = "No Courses Available."
            } else {
                courseData = data.reverse().map(courseData => {
                 
                    
                       
                    
                    return  (
                        `
                                <div class="col-md-6 my-3 bgCard">
                                    <div class="card shadow border border-secondary">
                                        <div class="card-body border-bottom border-secondary">
                                            <h5 class="card-title">${courseData.name}</h5>
                                            <p class="card-text text-left">${courseData.description}</p>
                                            <p class="card-text text-right">&#8369;${courseData.price}</p>
                                        </div>
                                        <div class="card-footer">
                                            
                                            <a href="./course.html?courseId=${courseData._id}" class="btn btn-primary text-dark btn-block buttonEffect">Go To Course</a>

                                            <a href="./editCourse.html?courseId=${courseData._id}" class="btn btn-primary text-white btn-block"> Update </a>
                                            
                                            <a href="./deleteCourse.html?courseId=${courseData._id}" class="btn btn-primary text-white btn-block ">Delete Course</a>


                                        </div>
                                    </div>
                                </div>
                        `   
                    )   
                }).join('')
                courseContainer.innerHTML = courseData;
            }
        })  
	
// If logged in as Student

} else {
	


	buttonControl.innerHTML = `<a href="./profile.html" class="btn btn-primary mt-3 buttonEffect">
      View Profile </a>`;

	fetch('https://rocky-harbor-60941.herokuapp.com/api/courses', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        }

    })

    .then(res => res.json())
    .then(data => {

    	if (data.length<1){
			courseData = "No Courses Available."
		};

        courseData = data.reverse().map(courseData => {
            
        	cardFooter = `<a href="./course.html?courseId=${courseData._id}" class="btn btn-primary text-white btn-block">Go To Course</a>`;

            return (
                `
				<div class="col-md-6 my-3">
						<div class="card shadow border border-secondary">
							<div class="card-body">
								<h5 class="card-title">${courseData.name}</h5>
								<p class="card-text text-left">${courseData.description}</p>
								<p class="card-text text-right">&#8369;${courseData.price}</p>
							</div>
							<div class="card-footer">${cardFooter}</div>
						</div>
					</div> 
				`)
        }).join("");


        courseContainer.innerHTML = courseData;
    })


// End of If Student
}


// End of If Logged In
}