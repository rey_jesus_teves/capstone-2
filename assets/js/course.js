let params = new URLSearchParams(window.location.search); 
let courseId = params.get('courseId');
let token = localStorage.getItem('token');

console.log("courseId: " + courseId);

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

fetch(`https://rocky-harbor-60941.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	if (isAdmin == "false") {


// if already enrolled tell: You are enrolled in this course.

fetch('https://rocky-harbor-60941.herokuapp.com/api/users/details', {
        method: "GET",
        headers: {
            "Authorization": `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(userData => {



        userData.enrollments.map(coursesEnrolled => {


            let courseidsEnrollled = coursesEnrolled.courseId;



            if (courseidsEnrollled.includes(data._id)) {

                enrollContainer.innerHTML = `<h5 class="text-white card-footer">You are enrolled in this course.  </h5>`

            }



        });
        // End Get User Data 
    })


// If unenrolled show enroll button
		            
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

		document.querySelector("#enrollButton").addEventListener("click",()=>{

			fetch('https://rocky-harbor-60941.herokuapp.com/api/users/enroll',{
				method:'POST',
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId

				})
			})
			.then(res => res.json())
			.then(data => {


				if(data){
					alert("You have enrolled successfully.")
					window.location.replace("./courses.html")
				} else {
					alert("Enrollment Failed.")
				}
			})
		})

	} 

	// if Admin login

	else if (isAdmin == "true") { 

	

		if(data.enrollees.length !== 0){
     				  
     		enrollContainer.innerHTML += `<h5 class="pt-5">Enrolled Student:</h5>` } 

     	else { 

     		enrollContainer.innerHTML += `<h5 class="pt-5">There Is No Enrollee In This Course Yet</h5>`

     	}
		
	
		data.enrollees.map(enrolleesInfo=>{

			//custom route was added on the default Heroku backend to be able to get user data by user id
			fetch(`https://rocky-harbor-60941.herokuapp.com/api/users/${enrolleesInfo.userId}`,{
					
					method: "GET",
					headers: {
			"Authorization": `Bearer ${token}` },
					
					
		})
				.then(res=>res.json())
				.then(data2=>{
					
					
				console.log(data2);

							enrollContainer.innerHTML +=
							`
								<div class="col-sm-12 my-3">
									<div clas="card">
										<div class="card-body border border-dark shadow rounded">
											<h6 class="card-title">Name: ${data2.firstName} ${data2.lastName}</h6>
											
										</div>
									</div>
								</div>
							`
						
					
				})
			
		}) 
	}
})
