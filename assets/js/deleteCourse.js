let params = new URLSearchParams(window.location.search); 
let courseId = params.get('courseId');
let token = localStorage.getItem('token');

fetch(`https://rocky-harbor-60941.herokuapp.com/api/courses/${courseId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        },

    })

    .then(res => res.json())
    .then(data => {
            console.log(data);
            if (data == true) {
                console.log("Success! You were able to delete the course from the list.");


                document.getElementById("deletedMessage").innerHTML = "Success! You were able to delete the course from the list.";

                document.getElementById("spinImg").style.visibility = "hidden";

                // window.location.replace("./courses.html");


            } else {
                alert("There was an when error when deleting");
            }
            // window.location.replace("./courses.html");
        }

    )