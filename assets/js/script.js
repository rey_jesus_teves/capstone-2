let navItems = document.querySelector("#zuitterNav");
let userToken = localStorage.getItem("token");
let isAdmin = localStorage.getItem("isAdmin");

if(!userToken) {
	navItems.innerHTML = 
	`
		<ul class="navbar-nav ml-auto">
		<li class="nav-item">
			<a href="./login.html" class="nav-link"> Log in </a>
		</li>
		
		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
		</ul>
	`
} else {
	
	navItems.innerHTML += 
	`
		<ul class="navbar-nav">
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>
		</ul>	
	`	
}

