let token = localStorage.getItem('token');

console.log(token);

let createForm = document.querySelector("#createCourse");

createForm.addEventListener("submit", (e) => {
    e.preventDefault();

    let courseName = document.querySelector("#courseName").value;
    let coursePrice = document.querySelector("#coursePrice").value;
    let courseDescription = document.querySelector("#courseDescription").value;




    fetch('https://rocky-harbor-60941.herokuapp.com/api/courses', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
                name: courseName,
                price: coursePrice,
                description: courseDescription

            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)
            alert("Course was added");
            //redirect to courses 
            window.location.replace("./courses.html")
        });

});