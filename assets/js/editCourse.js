// Edit course form
const editCourse = document.querySelector('#editCourse');

// Input boxes
const editName = document.querySelector('#courseName');
const editPrice = document.querySelector('#coursePrice');
const editDesc = document.querySelector('#courseDescription');

// Access Token
// let access = localStorage.getItem('token');
let token = localStorage.getItem('token');


// Get ID
// const queryId = window.location.search;

// const urlParams = new URLSearchParams(queryId);
let params = new URLSearchParams(window.location.search); 

// const id = urlParams.get('id')
let courseId = params.get('courseId');

let Id = document.querySelector('#courseId')

fetch('https://rocky-harbor-60941.herokuapp.com/api/users/details',

        {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
    .then(res => res.json())
    .then(data => {

        // if (data.isAdmin === true) {

            // Populate input boxes

            fetch(`https://rocky-harbor-60941.herokuapp.com/api/courses/${courseId}`, {
                    method: `GET`,
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })
                .then(res => res.json())
                .then(edit => {
                    editName.value = edit.name
                    editDesc.value = edit.description
                    editPrice.value = edit.price
                })



            // Button click

            editCourse.addEventListener('submit', (e) => {
                e.preventDefault()

                const name = document.querySelector('#courseName').value
                const description = document.querySelector('#courseDescription').value
                const price = document.querySelector('#coursePrice').value




                fetch(`https://rocky-harbor-60941.herokuapp.com/api/courses/`, {

                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${token}`

                        },
                        body: JSON.stringify({

                            name: name,
                            description: description,
                            price: price
                        })
                    })
                    .then(res => res.json())
                    .then(data => {

                        if ((name === '') || (description === '') && (price === '')) {
                            
                            Swal.fire({
                               icon: 'info',
                               title: 'Instruction',
                               text: 'Please fill in all the details to complete the update.',
                               showClass: {
                                   popup: 'animate__animated animate__fadeInDown'
                               },
                               hideClass: {
                                   popup: 'animate__animated animate__fadeOutUp'
                               }

                           })
                        } else {
                           
                            Swal.fire({
                               icon: 'success',
                               title: 'Good Job',
                               text: 'Success! You were able to update the subject details.',
                               showClass: {
                                   popup: 'animate__animated animate__fadeInDown'
                               },
                               hideClass: {
                                   popup: 'animate__animated animate__fadeOutUp'
                               }

                           })

                            // .then(function() {
                            //    window.location.href = "courses.html"; })


                        }

                    })


                fetch(`https://rocky-harbor-60941.herokuapp.com/api/courses/${courseId}`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${token}`
                        },

                    })

                    .then(res => res.json())
                    .then(data => {
                            
                            if (data == true) {
                                
                                console.log("Data was archived for the undo feature of the app.");

                                window.location.replace("./courses.html");


                            } else {
                                console.log("There was an error, contact your school Technical Support ");
                            }


                        }

                    )


            })

        // } else {
        //     alert("Please login as Admin.");


        // }

    })